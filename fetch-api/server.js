const path = require('path')
const fs = require('fs')
const express = require('express')
const { error } = require('console')
const app = express()
const PORT = process.env.PORT || 3000
const dataPath = path.join(__dirname, 'data')

app.use(express.json())

app.get('/api/users', (req, res) => {
  fs.readFile(dataPath + '/user.json', 'utf8', (error, data) => {
    res.set({ 'Content-type': 'application/json' })
    res.status(200)
    res.send(data)
  })
})

app.get('/api/text', (req, res) => {
  fs.readFile(dataPath + '/simple.txt', 'utf8', (error, data) => {
    res.set({ 'Content-type': ' text/plain ' })
    res.status(200)
    res.send(data)
  })
})

app.post('/api/users', (req, res) => {
  fs.readFile(dataPath + '/user.json', (error, data) => {
    var jsonData = JSON.parse(data)

    var postData = {
      id: jsonData.length + 1,
      first_name: req.body.first_name,
      email: req.body.email,
    }

    jsonData.push(postData)
    fs.writeFile(
      dataPath + '/user.json',
      JSON.stringify(jsonData),
      function (err) {
        if (err) throw err
        console.log('Added user in the database')
        res.set({ 'Content-type': 'application/json' })
        res.status(200)
        res.send(postData)
      }
    )
  })
})

// app.put('/api/users/:id', (req, res) => {
//     fs.readFile(dataPath + '/user.json', 'utf8', (error, data) => {

//         var jsonData = JSON.parse(data)
//         var findUser=jsonData.find()

//       var updatedData = {
//       id: jsonData.length + 1,
//       first_name: req.body.first_name,
//       email: req.body.email,
//     }
//       res.set({ 'Content-type': 'application/json' })
//       res.status(200)
//       res.send(updatedData)
//     })
//   })

app.use(express.static(path.join(__dirname, 'public')))

app.listen(PORT, () => {
  console.log(`Listening on port ${PORT}`)
})
