document.querySelector('#getUsers').addEventListener('click', getUser)

document.querySelector('#getPosts').addEventListener('click', getPost)

document.querySelector('#getText').addEventListener('click', getText)

document.querySelector('#addNewUser').addEventListener('click', addNewUser)

function getText() {
  fetch('/api/text')
    .then((res) => res.text())
    .then((data) => {
      document.querySelector('#output').innerHTML = `
            <p>${data}</p>
            `
    })
}

function getUser() {
  fetch('/api/users')
    .then((res) => res.json())
    .then((data) => {
      let output = '<h2>Users</h2>'
      data.forEach((user) => {
        output += `  
        <li>User Id: ${user.id}</li>
        <li>First Name: ${user.first_name}</li>
        <li> Email: ${user.email}</li>
        
        <br>
        <br>  
          `
      })
      document.querySelector('#output').innerHTML = output
    })
}

function getPost() {
  fetch('https://jsonplaceholder.typicode.com/posts')
    .then((res) => res.json())
    .then((data) => {
      let output = '<h1>Posts</h1>'
      data.forEach((post) => {
        output += `
        <h4>${post.title}</h4>
        <p>${post.body}</p>
          `
      })
      document.querySelector('#output').innerHTML = output
    })
    .catch((error) => {
      console.error(error)
    })
}

function addNewUser() {
  let output = `  <form id="addPost">
      <div>
        <input type="text" id="first_name" placeholder="First Name" />
      </div>
      <div>
        <input type="text" id="email" placeholder="Email" />
      </div>
      <button id="submit" >Submit</button>
    </form>`
  document.querySelector('#output').innerHTML = output
  document.querySelector('#submit').addEventListener('click', addUser)
}
function addUser(e) {
  e.preventDefault()

  let first_name = document.getElementById('first_name').value
  let email = document.getElementById('email').value
  let output = 'User Added'
  fetch('/api/users', {
    method: 'POST',
    headers: {
      'Content-type': 'application/json',
    },
    body: JSON.stringify({ first_name: first_name, email: email }),
  })
    .then((res) => res.json())
    .then((user) => {
      output += `
      <h4>${user.first_name}</h4>
      <p>${user.email}</p>
        `
      document.querySelector('#output').innerHTML = output
    })
}
