function lotteryDraw() {
  return new Promise((resolve, reject) => {
    let num = Math.floor(Math.random() * 10)
    if (num === 2) {
      resolve('You Won')
    } else {
      reject('You Lost')
    }
  })
}

lotteryDraw()
  .then((res) => {
    console.log(res)
  })
  .catch((err) => console.error(err))
