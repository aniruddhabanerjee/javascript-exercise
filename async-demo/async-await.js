let myPromise = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Yay, I resolved!')
    }, 1000)
  })
}

async function myAsync() {
  let a = await myPromise()

  console.log(a)
}
myAsync().then((res) => console.log(res))
