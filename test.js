function fib(n) {
  if (n < 2) return 1
  return fib(n - 1) + fib(n - 2)
}

function memoization(fn) {
  let cache = {}
  return function (n) {
    if (cache[n] != undefined) {
      return cache[n]
    } else {
      let res = fn(n)
      cache[n] = res
      return res
    }
  }
}

const memoFib = memoization(fib)
console.log(memoFib(552))
