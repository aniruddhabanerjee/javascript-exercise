// Call Back Method
// navigator.geolocation.getCurrentPosition((pos) => {
//   console.log(pos)
// })

function getLocation() {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(resolve, reject)
  })
}

getLocation()
  .then((pos) => {
    const { latitude: lat, longitude: lng } = pos.coords

    return fetch(`https://geocode.xyz/${lat},${lng}?geoit=json`)
  })
  .then((res) => {
    if (!res.ok) throw new Error('Problem with geocoding')
    res.json()
  })
  .then((data) => {
    return fetch(`https://restcountries.eu/rest/v2/name/${data.country}`)
  })
  .then((res) => res.json())
  .then((data) => console.log(data[1]))
